<?php
class Blog extends Controller{
    public function index() {
        $data['judul'] = "Blog";
        $data['blog'] = $this->model('Blog_model')->dataBlogAndUserJoin();
        $data['user'] = $this->model('User_model')->getAllUserData();
        $this->view("templates/header", $data);
        $this->view("blog/index", $data);
        $this->view("templates/footer");
    }

    public function detail($id)
    {
        $data['judul'] = "Blog Detail";
        $data['blog'] = $this->model('Blog_model')->getBlogJoinUserById($id);
        $this->view("templates/header", $data);
        $this->view("blog/detail", $data);
        $this->view("templates/footer");
    }

    public function add()
    {

        if ($this->model('Blog_model')->addBlogData($_POST) > 0) {
            header('Location: ' . BASE_URL . '/blog');
            exit;
        } else {
            header('Location: ' . BASE_URL . '/blog');
            exit;
        }
    }

    public function edit()
    {
        if ($this->model('Blog_model')->editBlogData($_POST) > 0) {
            header('Location: ' . BASE_URL . '/blog');
            exit;
        } else {
            header('Location: ' . BASE_URL . '/blog');
            exit;
        }
    }
    public function delete($id)
    {
        if ($this->model('Blog_model')->deleteBlogData($id) > 0) {
            header('Location: ' . BASE_URL . '/blog');
            exit;
        } else {
            header('Location: ' . BASE_URL . '/blog');
            exit;
        }
    }

}

?>