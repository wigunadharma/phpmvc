<?php

class Login extends Controller{

    public function index()
    {
        if(!isset($_SESSION['user']))
        {

        $data['judul'] = "Login";
        $this->view("templates/header", $data);
        $this->view("Login/index", $data);
        $this->view("templates/footer");        
        }
        else
        {
            header('Location: ' . BASE_URL);
            exit;
        }
    }

    public function sign() 
    {
        $userData = [
            'username' => $_POST['username'],
            'password' => $_POST['password'],
        ];

        $userData['password'] = md5($userData['password']);
        $userData['password'] .= SALT;

        // var_dump($userData);

        $userData = $this->model('User_model')->getUserByUname($userData);
        if(!$userData)
        {
            header('Location: ' . BASE_URL . '/login?msg=Username atau Password salah');
            exit;
        }
        else 
        {
            // reset default user session
            unset($_SESSION['user']);

            $_SESSION['user'] = [
                'id_user' => $userData['id_user'],
                'nama_user' => $userData['nama_user'],
                'username' => $userData['username'],
                'email' => $userData['email'],
                'password'=> $userData['password'],
            ];

            header('Location: ' . BASE_URL . '/index');
            exit;
        }
    }

    
}