<div class="container">
    <div class="row justify-content-center">
        <div class="col-8">
            <div class="card">
                <img src="https://placekitten.com/400/150" class="card-img-top img-fluid" alt="...">
                <div class="card-body">
                    <h5 class="card-title text-center mb-4"><?= $data['blog']['judul'];?></h5>
                    <h6 class="card-subtitle mb-3 text-muted">Ditulis Oleh : <span class="text-dark"><?= $data['blog']['nama_user'];?></span></h6>
                    <p class="card-text"><?= $data['blog']['tulisan'];?></p>
                </div>
            </div>
        </div>
    </div>
</div>