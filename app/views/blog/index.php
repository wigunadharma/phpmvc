
<nav class="navbar navbar-expand-lg bg-light">
  <div class="container">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ms-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>/index">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>/user">User</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL;?>/blog">Blog</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container">
    <div class="row">
      <div class="col-3 my-3">
        <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#createModal">Tambah</a>
      </div>
    </div>
    <div class="row">
        <div class="col table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Penulis</th>
                        <th scope="col">Judul</th>
                        <th scope="col">Text</th>
                        <th scope="col">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1;?>
                    <?php foreach($data['blog'] as $blog) :?>
                    <tr>
                        <th scope="row"><?= $i++; ?></th>
                        <td><?= $blog['username']; ?></td>
                        <td><?= $blog['judul']; ?></td>
                        <td><?= $blog['tulisan']; ?></td>
                        <td><a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editModal<?= $blog['id_blog'];?>">Edit</a> <a href="<?= BASE_URL; ?>/blog/delete/<?= $blog['id_blog']; ?>" class="btn btn-danger">Hapus</a> <a class="btn btn-warning" href="<?= BASE_URL; ?>/blog/detail/<?= $blog['id_blog']; ?>">Detail</a></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- =========== modal =========== -->
<div class="modal" id="createModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="<?= BASE_URL; ?>/blog/add" method="post">
          <div class="mb-3">
            <label for="" class="form-label">Penulis</label>
            <!-- <select class="form-select" name="id_user">
              <?php foreach ($data['user'] as $user) :?>
              <option value="<?= $user['id_user']; ?>"><?= $user['nama_user']; ?></option>
              <?php endforeach;?>
            </select> -->
            <input type="hidden" name="id_user" value="<?= $_SESSION['user']['id_user']; ?>">
            <input type="text" class="form-control" value="<?= $_SESSION['user']['username']; ?>" readonly>
          </div>
          <div class="mb-3">
            <label for="" class="form-label">Judul</label>
            <input type="text" class="form-control" name="judul">
          </div>
          <div class="mb-3">
            <label for="" class="form-label">Textarea</label>
            <textarea class="form-control" rows="5" name="tulisan"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
    </form>
    </div>
  </div>
</div>

<?php foreach($data['blog'] as $blog) :?>
<div class="modal" id="editModal<?= $blog['id_blog']?>">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="<?= BASE_URL; ?>/blog/edit" method="post">
        <input type="hidden" name="id_blog" value="<?= $blog['id_blog']; ?>">
          <div class="mb-3">
            <label for="" class="form-label">Penulis</label>
            <input type="hidden" name="id_user" value="<?= $_SESSION['user']['id_user']; ?>">
            <input type="text" class="form-control" value="<?= $_SESSION['user']['username']; ?>" readonly>
          </div>
          <div class="mb-3">
            <label for="" class="form-label">Judul</label>
            <input type="text" class="form-control" name="judul" value="<?= $blog['judul']; ?>">
          </div>
          <div class="mb-3">
            <label for="" class="form-label">Textarea</label>
            <textarea class="form-control" rows="5" name="tulisan"><?= $blog['tulisan']; ?></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Kirim</button>
      </div>
    </form>
    </div>
  </div>
</div>
<?php endforeach;?>
<!-- =========== akhir modal =========== -->