<nav class="navbar navbar-expand-lg bg-light">
  <div class="container">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ms-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>/index">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL; ?>/user">User</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL;?>/blog">Blog</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL;?>/logout">Keluar</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<section id="page-header">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col">
            <div id="page-header-box">
              <div class="col-xxl-6 col-xl-6 col-md-6 col-xs-6 col-6">
                <h1>Dashboard</h1>
              </div>
              <div class="col-xxl-6 col-xl-6 col-md-6 col-xs-6 col-6">
                <nav aria-label="breadcrumb" class="navbar-breadcrumb">
                  <ul class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="#">Main</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="dashboard-layouts">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-xxl-3 col-xl-3 col-md-3">
            <button id="btn-mobile-sidebar">Menu Dashboard</button>
            <section id="sidebar">
              <div class="sidebar-list">
                <h4>Main</h4>
                <ul class="sidebar-item">
                  <li class="sidebar-link">
                    <a href="">
                      <i class="fa fa-tachometer" aria-hidden="true"></i>
                      Dashboard
                    </a>
                  </li>
                  <li class="sidebar-link">
                    <a href="">
                      <i class="fa fa-tachometer" aria-hidden="true"></i>
                      Ubah Profil
                    </a>
                  </li>
                  <li class="sidebar-link">
                    <a href="">
                      <i class="fa fa-tachometer" aria-hidden="true"></i>
                      Ubah Kata Sandi
                    </a>
                  </li>
                  <li>
                    <hr />
                  </li>
                </ul>
              </div>
              <div class="sidebar-list">
                <h4>Undangan</h4>
                <ul class="sidebar-item">
                  <li class="sidebar-link">
                    <a href="">
                      <i class="fa fa-tachometer" aria-hidden="true"></i>
                      Buat Undangan
                    </a>
                  </li>
                  <li class="sidebar-link">
                    <a href="">
                      <i class="fa fa-tachometer" aria-hidden="true"></i>
                      Edit Undangan
                    </a>
                  </li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-xxl-9 col-xl-9 col-md-9">
            <section id="main-content">
              <div class="row justify-content-center">
                <div class="col">
                  <h3>Selamat datang, <span class="name-user">Adiwiguna</span></h3>
                </div>
              </div>
              <div class="row justify-content-center card-container">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
                  <div class="card card-list-container">
                    <a href="" class="text-decoration-none text-dark">
                      <div class="row justify-content-center align-items-center">
                        <div class="col-9">
                          <div class="card-body">
                            <h2 class="card-title">0</h5>
                            <p class="card-text">Total Undangan</p>
                          </div>
                        </div>
                        <div class="col-3">
                          <div class="card-icon">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                          </div>
                        </div>
                      </div>
                    </a>
                    <div class="bootom-card">
                      <a href="">
                        <p>Selengkapnya <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></p>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
                  <div class="card card-list-container">
                    <a href="" class="text-decoration-none text-dark">
                      <div class="row justify-content-center align-items-center">
                        <div class="col-9">
                          <div class="card-body">
                            <h2 class="card-title">0</h5>
                            <p class="card-text">Total Undangan</p>
                          </div>
                        </div>
                        <div class="col-3">
                          <div class="card-icon">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                          </div>
                        </div>
                      </div>
                    </a>
                    <div class="bootom-card">
                      <a href="">
                        <p>Selengkapnya <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></p>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
                  <div class="card card-list-container">
                    <a href="" class="text-decoration-none text-dark">
                      <div class="row justify-content-center align-items-center">
                        <div class="col-9">
                          <div class="card-body">
                            <h2 class="card-title">0</h5>
                            <p class="card-text">Total Undangan</p>
                          </div>
                        </div>
                        <div class="col-3">
                          <div class="card-icon">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                          </div>
                        </div>
                      </div>
                    </a>
                    <div class="bootom-card">
                      <a href="">
                        <p>Selengkapnya <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></p>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
                  <div class="card card-list-container">
                    <a href="" class="text-decoration-none text-dark">
                      <div class="row justify-content-center align-items-center">
                        <div class="col-9">
                          <div class="card-body">
                            <h2 class="card-title">0</h5>
                            <p class="card-text">Total Undangan</p>
                          </div>
                        </div>
                        <div class="col-3">
                          <div class="card-icon">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                          </div>
                        </div>
                      </div>
                    </a>
                    <div class="bootom-card">
                      <a href="">
                        <p>Selengkapnya <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></p>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row justify-content-center">
                <div class="col">
                  <div id="page-header-box">
                    <div class="col-12">
                      <h2>Aktivitas terbaru</h2>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
    </section>


<div class="container">
    <div class="row">
        <h1>home</h1>
        <!-- <p>user yang login <?= $_SESSION['user']; ?></p> -->
        <?php
        var_dump($_SESSION['user']);
        
        ?>
    </div>
</div>