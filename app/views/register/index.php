<div class="container">
    <div class="row justify-content-center">
        <div class="col-6 my-auto mt-5">
            <div class="title mb-3 ">
                <h6>Register</h6>
            </div>
            <form action="<?= BASE_URL; ?>/register/sign" method="post">
                <div class="mb-3">
                <label for="nama_user" class="form-label">nama</label>
                <input type="text" class="form-control" id="nama_user" name="nama_user" require>
                </div>
                <div class="mb-3">
                <label for="username" class="form-label">username</label>
                <input type="text" class="form-control" id="username" name="username" require>
                </div>
                <div class="mb-3">
                <label for="email" class="form-label">email</label>
                <input type="text" class="form-control" id="email" name="email" require>
                </div>
                <div class="mb-3">
                <label for="password" class="form-label">password</label>
                <input type="password" class="form-control" id="password" name="password" require>
                </div>
                <button type="submit" class="btn btn-secondary"> Kirim</button>
            </form>
        </div>
    </div>
</div>