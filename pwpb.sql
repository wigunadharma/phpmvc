-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 11, 2022 at 04:35 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pwpb`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id_blog` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `tulisan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id_blog`, `id_user`, `judul`, `tulisan`) VALUES
(1, 2, 'judul satu edit', 'content satu edit'),
(2, 1, 'test admin', 'test admin'),
(3, 1, 'test2', 'test2'),
(4, 1, 'judul2', 'test2'),
(5, 1, 'Cara membuat rumah dari katik es krim', 'Alat dan Bahan Membuat Rumah dari Stik Es Krim  Cara membuat rumah dari stik es krim yang paling pertama adalah menyiapkan alat dan bahan.   Beberapa bahan yang kamu butuhkan untuk membuat miniatur rumah dari stik ini adalah sebagai berikut:  Kertas karton atau kardus  Stik es krim (kurang lebih 100 buah)  Gunting  Penggaris  Cutter  Lem tembak  Lem kayu  Cat (opsional)  Ornamen miniatur rumah (opsional)  Setelah bahan dan alat yang diperlukan sudah lengkap, sekarang kamu siap untuk mengerjakan prakarya tersebut.   Cara membuat rumah dari stik sebenarnya mudah, berikut langkah-langkah yang bisa diikuti.  Langkah-Langkah Membuat Rumah dari Stik Es Krim  Mula-mula, letakkan kertas karton atau kardus sebagai pelapis agar lem tidak merusak permukaan lantai. Lalu, cara membuat rumah dari stik es krim yang mudah bisa dimulai dengan membuat bagian dinding rumah.  Susun stik es krim membentuk segi empat dengan 4 buah stik.c Buatlah sebanyak 4 buah. Beri lem di setiap sudutnya. Empat buah segi empat stik es krim ini akan menjadi kerangka dinding miniatur rumah nantinya.  Setelah kerangka atau bingkainya jadi, tutup bingkai tersebut dengan susunan stik es krim lainnya. Beri lem pada setiap bagian ujungnya sehingga kokoh dan tidak bercelah alias rapat.  Jika stik terakhir tidak muat karena kelebihan, kamu bisa memotongnya dengan cutter. Ulangi cara membuat rumah dari stik di atas pada setiap kerangka miniatur rumah yang tersisa.  Membuat Atap Rumah dari Stik Es Krim  Selanjutnya, kamu bisa melanjutkan cara membuat rumah dari stik dengan merangkai bagian atapnya. Buat 3 buah kerangka segitiga lalu beri lem pada sudut-sudutnya agar merekat kuat. Dua buah segitiga akan menjadi sisi depan dan belakang atap miniatur rumah dari stik nantinya.  Sedangkan, satu kerangka segitiga sisanya bisa kamu letakkan di tengah agar atap miniatur rumah dari stik semakin kokoh dan awet. Pada kerangka tengah, letakkan 10 stik secara mendatar kemudian beri lem agar merekat.   Cara membuat miniatur rumah selanjutnya adalah dengan menempelkan dua kerangka segitiga yang awal pada sisi atas dan bawah stik mendatar dengan posisi berdiri. Sehingga model atap pelana akan terbentuk.   Membuat Badan Rumah dari Stik Es Krim  Cara membuat rumah dari stik es krim berikutnya sampai pada badan rumah. Dinding miniatur rumah dari stik yang sudah jadi dipotong bagian jendela dan pintunya. Kamu bisa menggunakan alat bantu penggaris dan cutter untuk memotongnya bagian jendela dan pintu.  Setelah dinding sudah memiliki jendela dan pintu, kamu tinggal merakit dinding-dinding tersebut menjadi satu miniatur rumah dari stik. Gunakan lem yang cukup agar setiap dindingnya merekat dengan baik. Kamu juga harus sedikit berhati-hati agar dindingnya tidak miring.  Setelah dinding miniatur rumah tegak berdiri, cara membuat miniatur dari stik es krim berikutnya adalah dengan memasang atap. Beri lem pada ujung setiap stik es krim dan pasang dengan hati-hati. Jika kamu ingin membuat hiasan pada bagian dalam rumah, lakukan sebelum memasang atap, ya!  Tahap Finishing Rumah dari Stik Es Krim  Cara membuat rumah dari stik es krim yang terakhir adalah bagian finishing. Di sini, kamu bisa mengecat alias memberi warna miniatur rumah kamu sesuai selera. Jika kamu memilih untuk tidak mengecatnya, miniatur rumah dari stik ini akan tetap terlihat cantik dengan warna natural.  Kamu juga bisa menambahkan halaman pada rumah miniatur ini sehingga nampak seperti rumah asli. Misalnya, kamu bisa tambahkan ornamen tumbuhan, replika batu koral, pepohonan, bahkan, kamu bisa menambahkan ornamen pagar kayu untuk melengkapi halaman miniatur rumah dari stik ini.');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nama_user`, `username`, `email`, `password`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', 'admin'),
(2, 'adnyaganteng', 'adnyasutha', 'adnyasutha@gmail.com', 'adnya123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id_blog`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id_blog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blog`
--
ALTER TABLE `blog`
  ADD CONSTRAINT `blog_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
